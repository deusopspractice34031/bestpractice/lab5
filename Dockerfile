FROM alpine as system

ARG BUILDKIT_INLINE_CACHE=1


FROM golang:alpine as build

ARG BUILDKIT_INLINE_CACHE=1

WORKDIR /app

COPY ../hello_serv.go . 

RUN go mod init serv

RUN go mod download serv

RUN go build -o /app/build

COPY ../start.sh .


FROM scratch as app

COPY --from=system /bin  /bin
COPY --from=system /sbin /sbin
COPY --from=system /lib  /lib

COPY --from=build /app/build    /build/build
COPY --from=build /app/start.sh ../config/start.sh

ENTRYPOINT [ "/bin/sh"]

CMD [ "/config/start.sh" ]